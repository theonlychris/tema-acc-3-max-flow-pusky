#pragma once
#include <sstream>
#include <fstream>
#include "Utils.hpp"
#include "Arc.h"
#include "Node.h"
class Graph
{
public:
	Graph();
public:
	void readFromFile(const std::string & fileName);
	void GetMaxFlow();
public:
	std::vector<Node> senators;
	std::vector<Node> commissions;
	std::vector<Arc> arcs;
	std::vector<Arc> resultAfterMaxFlow;
	std::vector<Arc> sourceArcs;
	int maxFlow = 2;
};

