#pragma once
#include <iostream>
#include <string>

class Node
{
public:
	Node() = default;
	Node(const std::string& name);
	Node(const Node& otherNode);
public:
	friend std::ostream& operator <<(std::ostream& os, const Node& node);
	// friend bool operator ==(const Node& node);
public:
	std::string name;
	bool visited = 0;
	int uniqueCount = 0;
};

