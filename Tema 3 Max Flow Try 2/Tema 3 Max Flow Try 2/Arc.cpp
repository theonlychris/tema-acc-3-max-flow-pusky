#include "Arc.h"

Arc::Arc(const Node& first, const Node& second, int flow) : first(first), second(second), flow(flow)
{
	maxCapacity = 1;
	//Empty
}

Arc::Arc(const Arc& otherArc)
{
	first = otherArc.first;
	second = otherArc.second;
	flow = otherArc.flow;
	maxCapacity = otherArc.maxCapacity;
	residualFlow = otherArc.residualFlow;
}

std::ostream& operator<<(std::ostream& os, const Arc& arc)
{
	os << arc.first << " -> " << arc.second << " |> " << arc.maxCapacity << std::endl;
	return os;
}
