#pragma once
#include "Node.h"
class Arc
{
public:
	Arc() = default;
	Arc(const Node& first, const Node& second, int flow);
	Arc(const Arc& otherArc);
public:
	friend std::ostream& operator << (std::ostream& os, const Arc& arc);
public:
	Node first;
	Node second;
	int flow;
	int maxCapacity = 1;
	int residualFlow = 0;
};

