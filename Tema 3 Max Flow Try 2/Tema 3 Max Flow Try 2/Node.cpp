#include "Node.h"

Node::Node(const std::string& name) : name(name)
{
	// Empty
}

Node::Node(const Node& otherNode)
{
	name = otherNode.name;
}

std::ostream& operator<<(std::ostream& os, const Node& node)
{
	os << node.name;
	return os;
}

