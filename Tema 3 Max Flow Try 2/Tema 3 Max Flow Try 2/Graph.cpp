#include "Graph.h"

Graph::Graph()
{
	readFromFile("input.txt");
}

void Graph::readFromFile(const std::string& fileName)
{
	std::ifstream inputFile(fileName);
	int numberOfSenators;
	int numberOfCommissions;
	inputFile >> numberOfSenators;
	inputFile >> numberOfCommissions;


	for (int i = 0; i < numberOfSenators; ++i)
	{
		senators.push_back(Node("Senator " + std::to_string(i + 1)));
	}

	for (int i = 0; i < numberOfCommissions; ++i)
	{
		commissions.push_back(Node("Commission " + std::to_string(i + 1)));
	}

	int currentCommissionIndex = 0;

	int globalIndex = 0;
	while (currentCommissionIndex <= numberOfCommissions)
	{
		int senatorNumber;
		std::string line;
		std::getline(inputFile, line);
		std::istringstream endOfLine(line);
		int counter = 0;
		while (endOfLine >> senatorNumber)
		{
			arcs.push_back(Arc(senators[senatorNumber - 1], commissions[currentCommissionIndex - 1], 0));
			//counter++;
			//globalIndex++;
		}
		/*
		if (counter != 0)
		{
		arcs[globalIndex - counter].second.uniqueCount = counter;
		}
		*/
		currentCommissionIndex++;
	}
	std::cout << senators << std::endl;
	std::cout << commissions << std::endl;
	std::cout << arcs << std::endl;
}

void Graph::GetMaxFlow()
{
	/* Nodul sursa */
	Node sourceNode("sursa");
	Arc sourceArc(sourceNode, Node(""), 1);
	sourceArc.maxCapacity = 3;
	sourceArc.flow++;
	/* Doing a DFS Search */ 
	for (int j = 0; j < arcs.size(); j++)
	{
		if (!arcs[j].second.visited && !arcs[j].first.visited)
		{
			if(sourceArc.maxCapacity > sourceArc.flow)
			{ 
			sourceArc.second = arcs[j].second;
			sourceArc.flow++;
			arcs[j].flow++;
			resultAfterMaxFlow.push_back(arcs[j]);
			arcs[j].second.visited = true;
			/*
			for (int i = 0; i < arcs[j].second.uniqueCount; ++i)
			{
				arcs[j + i].second.visited = true;
			}
			*/

			for (int i = 0; i < arcs.size(); ++i)
			{
				if (arcs[j].first.name == arcs[i].first.name)
					arcs[i].first.visited = 1;
				if (arcs[j].second.name == arcs[i].second.name)
					arcs[i].second.visited = 1;
			}
			sourceArcs.push_back(sourceArc);
			sourceArc.flow--;
			}
		}
	}

	std::cout << "Source arcs: " << sourceArcs << std::endl;

	/* Showing the results */
	std::cout << "Max Flow is: " << ++maxFlow << std::endl;
	for (Arc a : resultAfterMaxFlow)
	{
		std::cout << a.first << " is the president of the " << a.second << std::endl;
	}


	for (int i = 0; i < arcs.size(); ++i)
	{
		if (arcs[i].second.visited == false)
			std::cout << arcs[i].second.name << " can't have a presedint " << std::endl;
	}
	
}

