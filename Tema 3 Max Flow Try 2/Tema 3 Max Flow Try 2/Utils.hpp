#pragma once
#include <iostream>
#include <vector>
#include <string>


/* Overloads the << Operator to show the contents of a vector */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<T>& myVector)
{
	os << "[";
	for (int i = 0; i < myVector.size(); ++i) {
		os << myVector[i];
		if (i != myVector.size() - 1)
			os << ", ";
	}
	os << "]" << std::endl;
	return os;
}